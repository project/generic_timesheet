<!-- Beginning of JavaScript -
// Math.round 

function hours (form) {
  todays_hours = (
    (
    ( (document.today.fh.options[document.today.fh.selectedIndex].value +
      (document.today.fm.options[document.today.fm.selectedIndex].value / 6)) 
    - (document.today.sh.options[document.today.sh.selectedIndex].value +
      (document.today.sm.options[document.today.sm.selectedIndex].value / 6))
  ) - (document.today.bh.options[document.today.bh.selectedIndex].value +
      (document.today.bm.options[document.today.bm.selectedIndex].value / 6))
  ) / 10);
  if ( document.today[2].checked == true ) {
	todays_hours = ( todays_hours + 24 );
  }
  x100 = (todays_hours * 100);
  rounded = ( (x100 - (x100 % 1)) / 100 );
  if ( ( x100 % 1 ) >= .5 ) {
	  // Round up if the remainder is greater than half
	  rounded = rounded + .01;
  }
  if ( todays_hours != rounded  ) {
	  // alert ( todays_hours + " => " + rounded  );
	  self.status = "Rounding " + todays_hours + " to " + rounded;
  }
  document.today.worked.value = rounded;
}

function update_total (form) {
 total_hours = (
 // Multiplication converts text strings into numerical values
   ( document.week.day0.value * 1 )
 + ( document.week.day1.value * 1 )
 + ( document.week.day2.value * 1 )
 + ( document.week.day3.value * 1 )
 + ( document.week.day4.value * 1 )
 + ( document.week.day5.value * 1 )
 + ( document.week.day6.value * 1 ));
 document.week.total.value = (total_hours);
}

function update_day (day) {
    if ( day == 0 ) {
    	document.week.day0.value = (document.today.worked.value);
    }
    if ( day == 1 ) {
    	document.week.day1.value = (document.today.worked.value);
    }
    if ( day == 2 ) {
    	document.week.day2.value = (document.today.worked.value);
    }
    if ( day == 3 ) {
    	document.week.day3.value = (document.today.worked.value);
    }
    if ( day == 4 ) {
    	document.week.day4.value = (document.today.worked.value);
    }
    if ( day == 5 ) {
    	document.week.day5.value = (document.today.worked.value);
    }
    if ( day == 6 ) {
    	document.week.day6.value = (document.today.worked.value);
    }
	update_total();
}
// - End of JavaScript - -->
